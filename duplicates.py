#!/usr/bin/python3

import sys
import os
import stat
import hashlib
import json

# TODO do not show empty files when listing individual files duplicates; still treat them as identical for the purposes of directories

if len(sys.argv) < 2:
	print("Usage: {} <path> ...\nFinds regular files which are exact duplicates of each other".format(sys.argv[0]))
	exit()

class Entry():
	def __init__(self, path, size):
		start = path.rfind("/") + 1

		self.name = path[start:]
		self.size = size
		self.hashsum = None
		self.paths = [path]

	def calchash(self):
		assert len(self.paths) == 1, "Entry contains {} improperly merged paths"

		# TODO what if the file doesn't exist anymore?

		f = open(self.paths[0], "rb")
		content = f.read()
		f.close()
		h = hashlib.sha256()
		h.update(content)
		self.hashsum = h.hexdigest()
		return self.hashsum

	def ready(self):
		return self.hashsum != None

	def key(self):
		if not self.hashsum:
			self.calchash()
		return self.hashsum + "." + self.name

	def merge(self, other):
		self.paths += other.paths

	def __repr__(self):
		return self.name + " [" + str(len(self.paths)) + "]"

counts = {}
redundant = {}

def merge(entries, other):
	# If there are multiple files with the same name but different hashes, the element where they are stored will be transformed into list where each element corresponds to a single hash

	if type(other) != list:
		other = [other]
	for entry in other:
		key = entry.size
		if key in entries:
			newkey = entry.key()
			if (type(entries[key]) == list):
				for item in entries[key]:
					if item.key() == newkey:
						item.merge(entry)
						break
				else:
					entries[key].append(entry)
				return

			if (entries[key].key() == newkey):
				entries[key].merge(entry)
			else:
				entries[key] = [entries[key], entry]
		else:
			entries[key] = entry

def mergedict(a, b):
	result = a
	for key in b.keys():
		merge(result, b[key])
	return result

def browse(directory):
	global counts
	result = {}
	counts[directory] = 0
	for item in os.listdir(directory):
		if (item[0] == "."):
			continue
		path = directory + "/" + item
		info = os.lstat(path)
		if stat.S_ISREG(info.st_mode):
			entry = Entry(path, info.st_size)
			merge(result, entry)
		elif stat.S_ISDIR(info.st_mode):
			result = mergedict(result, browse(path))

		counts[directory] += 1
	return result

def duplicates_foreach(entries, callback):
	for key in entries:
		if (type(entries[key]) == list): # identical size, different hash
			for item in entries[key]:
				if (len(item.paths) > 1):
					callback(item.paths)
		elif (len(entries[key].paths) > 1):
			callback(entries[key].paths)

def match_directory(l):
	global redundant
	for f in l:
		directory = os.path.dirname(f)
		if directory in redundant:
			redundant[directory] += 1
		else:
			redundant[directory] = 1

entries = {}
for path in sys.argv[1:]:
	path = os.path.realpath(path)
	entries = mergedict(entries, browse(path))

duplicates_foreach(entries, lambda s: print(json.dumps(s)))
duplicates_foreach(entries, match_directory)

for directory in redundant:
	if counts[directory] == redundant[directory]:
		print("all in \"{}\" can be found in other directories".format(directory))
